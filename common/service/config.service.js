let BASE_URL = ''
 let BASE_URLS = 'https://localhost.com'
// let BASE_URLS = 'http://localhost:9888'
if (process.env.NODE_ENV == 'development') {
	 BASE_URL = 'http://localhost:8091/jeecg-boot'
    //BASE_URL = 'http://192.168.10.106:8091/jeecg-boot' // 开发环境
} else {
	 BASE_URL = 'https://更换为正式环境地址' // 生产环境
}
let staticDomainURL = BASE_URL+ '/sys/common/static';

const configService = {
	apiUrl: BASE_URL,
	staticDomainURL: staticDomainURL,
	BASE_URLS:BASE_URLS,
};

export default configService
